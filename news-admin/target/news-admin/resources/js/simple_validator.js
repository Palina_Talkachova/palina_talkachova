var authorError;
var tagError;
var commentError;

function validateAuthor() {
	var authorName = document.getElementById('authorName').value;

	const
	AUTHOR_NAME_PATTERN = /[A-Z0-9][\w\s,.!?&-_<>*'\"\/()]{2,29}/;
	
	if (!AUTHOR_NAME_PATTERN.test(authorName)) {
		document.getElementById('editauthor-message').innerHTML = authorError;
		return false;
	}
	return true;
}

function validateTag() {
	var tagName = document.getElementById('tagName').value;
	const
	TAG_NAME_PATTERN = /[A-Z0-9][\w\s,.!?&-_<>*'\"\/()]{2,29}/;
	if (!TAG_NAME_PATTERN.test(tagName)) {
		document.getElementById('edittag-message').innerHTML = tagError;
		return false;
	}
	return true;
}

function validateComment() {
	var comment = document.getElementById('commentText').value;
	const
	COMMENT_PATTERN = /[A-Z0-9][\w\s,.!?&-_<>*'\"\/()]{2,99}/;
	if (!COMMENT_PATTERN.test(comment)) {
		document.getElementById('comment-text-error').innerHTML = commentError;
		return false;
	}
	return true;
}