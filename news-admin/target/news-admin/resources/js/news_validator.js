var titleError;
var shortTextError;
var fullTextError;
var dateError;

function makeValidation() {
	return validate(createNews());
}

/* determines fields for News object */
function News() {
	this._title;
	this._shortText;
	this._fullText;
	this._creationDate;
	this._modificationDate;
}

News.prototype.title = function(title) {
	if (typeof title != 'undefined') {
		this._title = title;
	}
	return this._title;
}

News.prototype.shortText = function(shortText) {
	if (typeof shortText != 'undefined') {
		this._shortText = shortText;
	}
	return this._shortText;
}

News.prototype.fullText = function(fullText) {
	if (typeof fullText != 'undefined') {
		this._fullText = fullText;
	}
	return this._fullText;
}

News.prototype.creationDate = function(creationDate) {
	if (typeof creationDate != 'undefined') {
		this._creationDate = creationDate;
	}
	return this._creationDate;
}

News.prototype.modificationDate = function(modificationDate) {
	if (typeof modificationDate != 'undefined') {
		this._modificationDate = modificationDate;
	}
	return this._modificationDate;
}

/* gets information about a news and creates News object */
function createNews() {
	var news = new News();
	news.title(document.getElementById('news.title').value);
	news.creationDate(document.getElementById('news.creationDate').value);
	news
			.modificationDate(document.getElementById('news.modificationDate').value);
	news.shortText(document.getElementById('news.shortText').value);
	news.fullText(document.getElementById('news.fullText').value);
	return news;
}

function validate(news) {
	var flag = true;
	/* constants */
	/*
	 * \s - space \w - letter, number, underscore \d - number
	 */
	const TITLE_PATTERN = /[A-Z0-9][\w\s,.!?&-_<>*'"\\/()]{2,29}/;
	const SHORT_TEXT_PATTERN = /[A-Z0-9][\w\s,.!?&-_<>*'\\"\/()]{2,99}/;
	const FULL_TEXT_PATTERN = /[A-Z0-9][\w\s,.!?&-_<>*'\"\/()]{2,199}/;
	const DATE_PATTERN = /[\d]{2}\/[\d]{2}\/[\d]{4}/;
	
	/* test is used to check if the regular expression matches the given string */
	if (!TITLE_PATTERN.test(news.title())) {
		flag = false;
		/*displays the text about an error if the regular expression matches the given string*/
		document.getElementById('title-error').innerHTML = titleError;
	} else {
		document.getElementById('title-error').innerHTML = null;
	}
	if (!SHORT_TEXT_PATTERN.test(news.shortText())) {
		flag = false;
		document.getElementById('short-text-error').innerHTML = shortTextError;
	} else {
		document.getElementById('short-text-error').innerHTML = null;
	}
	if (!FULL_TEXT_PATTERN.test(news.fullText())) {
		flag = false;
		document.getElementById('full-text-error').innerHTML = fullTextError;
	} else {
		document.getElementById('full-text-error').innerHTML = null;
	}
	if ((!DATE_PATTERN.test(news.creationDate()))
			|| (!DATE_PATTERN.test(news.modificationDate()))) {
		flag = false;
		document.getElementById('date-error').innerHTML = dateError;
	} else {
		document.getElementById('date-error').innerHTML = null;
	}
	return flag;
}