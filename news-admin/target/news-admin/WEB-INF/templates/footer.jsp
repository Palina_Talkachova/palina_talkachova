<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<footer id="footer">
	&copy;
	<fmt:message key="footer.copyright" />
</footer>