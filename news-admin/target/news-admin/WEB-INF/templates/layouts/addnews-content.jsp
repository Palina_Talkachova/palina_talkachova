<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!-- displays information about validation errors for client part of application-->
<script type="text/javascript">
	titleError = '<fmt:message key="title.error.size" />';
	shortTextError = '<fmt:message key="shorttext.error.pattern" />';
	fullTextError = '<fmt:message key="fulltext.error.pattern" />';
	dateError = '<fmt:message key="date.error" />';
</script>
<div id="content">
<!-- the event "onsubmit" (script) occurs when the form is sent -->
<!-- the modelAttribute is gotten from add news Controller -->
<sf:form method="POST" modelAttribute="newsValue"
		onsubmit="return makeValidation()" id="newsForm">
		<sf:hidden path="news.modificationDate" />
		<div id="addnews-title">
			<div>
				<fmt:message key="addnews.title" />
			</div>
			<div>
			<!-- the path binds inputed information and fields of newsValue object -->
				<sf:input path="news.title" /> 
			</div>
			<div class="addnews-error-message" id="title-error">
			<!-- BindingResult helps find errors during being inputed information in the form.
			With sf:errors is informed about validation error in title field -->
				 <sf:errors path="news.title" /> 
			</div>
		</div>
		<div id="addnews-date">
			<div>
				<fmt:message key="addnews.date" />
			</div>
			<div>
				 <sf:input path="news.creationDate" />
			</div>
			<div class="addnews-error-message" id="date-error">
			<!-- informs about validation error in creation date field -->
				<sf:errors path="news.creationDate" /> 
			</div>
		</div>
		<div id="addnews-short-text">
			<div>
				<fmt:message key="addnews.shorttext" />
			</div>
			<div>
				<sf:textarea rows="3" cols="50" path="news.shortText" />
			</div>
			<div class="addnews-error-message" id="short-text-error">
			<!-- informs about validation error in short text field -->
				 <sf:errors path="news.shortText" />
			</div>
		</div>
		<div id="addnews-full-text">
			<div>
				<fmt:message key="addnews.fulltext" />
			</div>
			<div>
				<sf:textarea rows="5" cols="50" path="news.fullText" />
			</div>
			<div class="addnews-error-message" id="full-text-error">
			<!-- informs about validation error in full text field -->
				 <sf:errors path="news.fullText" /> 
			</div>
		</div>
		<div id="search">
			<div id="authors">
			<!-- the form is for selecting one author for the news -->
				<sf:select id="ss" items="${authorList}" itemLabel="authorName"
					itemValue="authorId" path="author.authorId"></sf:select>
			</div>
			<!-- the form is for multiple selecting of tags for the news -->
			<div id="tags">
				<sf:select id="ms" multiple="multiple" items="${tagList}"
					itemLabel="tagName" itemValue="tagId" path="tags">
				</sf:select>
			</div>
			<!-- scripts allows to multiple select tags multiply -->
			<script>
				$('#ms').multipleSelect();
			</script>
		</div>
		<input id="save-news-button" type="submit" value="<fmt:message key="addnews.save" />" />
	</sf:form>
</div>
<div style="clear: both"></div>