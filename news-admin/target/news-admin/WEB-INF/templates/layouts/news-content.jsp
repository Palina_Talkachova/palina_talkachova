<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<script type="text/javascript">
	commentError = '<fmt:message key="commenttext.error.pattern" />';
</script>
<div id="content">
	<div id="news-backButton">
		<a href="<c:url value="/admin/newslist" />"><fmt:message
				key="back.button" /></a>
	</div>
	<div class="news">
		<div class="news-title">
			<strong>${ctg:escapeHtml4(news.news.title) }</strong>
		</div>
		<div class="news-author">
			(
			<fmt:message key="news.author" />
			${ctg:escapeHtml4(news.author.authorName) })
		</div>
		<div class="news-date">
			<fmt:formatDate pattern="dd-MM-yyy "
				value="${news.news.modificationDate}" />
		</div>
		<div style="clear: both"></div>
		<div class="news-full-text">${ctg:escapeHtml4(news.news.fullText) }</div>
	</div>
	<div id="comments">
		<c:forEach items="${news.comments}" var="comment">
			<div class="comment">
				<div class="comment-date">
					<fmt:formatDate pattern="dd-MM-yyy HH:mm:ss"
						value="${comment.creationDate}" />
				</div>
			</div>
			<div class="comment-text">${ctg:escapeHtml4(comment.commentText) }</div>
			<div class="delete-comment-button">
			<%-- deletes a comment --%>
				<form method="POST" action="?delete">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" /> <input name="commentId" type="hidden"
						value="${comment.commentId }" /> <input name="newsId"
						type="hidden" value="${news.news.newsId }" /> <input
						type="submit" value="<fmt:message key="news.delete" />" />
				</form>
			</div>
			<div style="clear: both"></div>
		</c:forEach>
		<%--the form is sent to Comment Controller--%>
		<sf:form modelAttribute="comment" method="POST" action="?add"
			onsubmit="return validateComment()">
			
			<%-- <sf:hidden path="creationDate" />
			<sf:hidden path="newsId" />  --%>
			<div id="write-comment">
			<%-- creates  a comment --%>
				<sf:textarea rows="5" cols="35" path="commentText" />
			</div>
			<div id="post-comment-button">
				<input type="submit"
					value="<fmt:message key="post.comment.button" />" />
			</div>
			<div id="comment-text-error" class="addnews-error-message">
			<%--displays validation error from the server part--%>
				<sf:errors path="commentText" />
			</div>
		</sf:form>
	</div>
	<div style="clear: both"></div>
	<div id="prevButton">
		<c:if test="${not empty prevNews}">
			<a href="<c:url value="/admin/news/${prevNews.newsId }" />"><fmt:message
					key="prev.button" /></a>
		</c:if>
	</div>
	<div id="nextButton">
		<c:if test="${not empty nextNews}">
			<a href="<c:url value="/admin/news/${nextNews.newsId }" />"><fmt:message
					key="next.button" /></a>
		</c:if>
	</div>
</div>
<div style="clear: both"></div>