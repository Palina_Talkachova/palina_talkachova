<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%-- The list attribute is first converted into a scripting variable; after 
			that it is iterated using the <c:forEach> tag. The compound attributes are 
			then rendered one after the other  --%>
<tiles:importAttribute name="javascripts" />
<tiles:importAttribute name="stylesheets" />
<%-- sets the name of the page--%>
<tiles:importAttribute name="title" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="${title }" /></title>
<%-- gets css styles --%>
<c:forEach var="css" items="${stylesheets}">
	<link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
</c:forEach>
<%-- gets java scripts --%>
<c:forEach var="script" items="${javascripts}">
	<script src="<c:url value="${script}"/>"></script>
</c:forEach>
</head>
<body>
	<!-- assembles pages -->
	<tiles:insertAttribute name="header" />
	<div id="side-and-content">
		<tiles:insertAttribute name="sidebar" />
		<!-- sets the content depend on the necessary content-jsp -->
		<tiles:insertAttribute name="content" />
	</div>
	<tiles:insertAttribute name="footer" />
</body>
</html>