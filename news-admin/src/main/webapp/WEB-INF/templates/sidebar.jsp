<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<aside id="sidebar-decoration">
	<div id="sidebar-list">
		<div class="sidebar-link">
			<c:choose>
			<%--sideBarPage attribute is gotten from a controller --%>
				<c:when test="${sideBarPage eq 'newsList' }">
					&rarr; <a style="font-weight: bold" href="<c:url value="/" />"><fmt:message
							key="sidebar.newslist" /></a>
				</c:when>
				<c:otherwise>
					&rarr; <a href="<c:url value="/" />"><fmt:message
							key="sidebar.newslist" /></a>
				</c:otherwise> 
			</c:choose>
		</div>
		<div class="sidebar-link">
			<c:choose>
				<c:when test="${sideBarPage eq 'addNews' }">
					&rarr; <a style="font-weight: bold"
						href="<c:url value="/admin/view/addnews" />"><fmt:message
							key="sidebar.addnews" /></a>
				</c:when>
				<%-- sends to a controller which manage the current URL  --%>
				 <c:otherwise>
					&rarr; <a href="<c:url value="/admin/view/addnews" />"><fmt:message
							key="sidebar.addnews" /></a>
				</c:otherwise> 
			</c:choose>
		</div>
		<div class="sidebar-link">
			<c:choose>
				<c:when test="${sideBarPage eq 'authors' }">
					&rarr; <a style="font-weight: bold"
						href="<c:url value="/admin/view/editauthors" />"><fmt:message
							key="sidebar.author" /></a>
				</c:when>
				<c:otherwise>
					&rarr; <a href="<c:url value="/admin/view/editauthors" />"><fmt:message
							key="sidebar.author" /></a>
				</c:otherwise> 
			</c:choose>
		</div>
		<div class="sidebar-link">
			<c:choose>
				<c:when test="${sideBarPage eq 'tags' }">
					&rarr; <a style="font-weight: bold"
						href="<c:url value="/admin/view/edittags" />"><fmt:message
							key="sidebar.tag" /></a>
				</c:when>
				<c:otherwise>
					&rarr; <a href="<c:url value="/admin/view/edittags" />"><fmt:message
							key="sidebar.tag" /></a>
				</c:otherwise> 
			</c:choose>
		</div>
	</div>
</aside>