<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<div id="content">
	<div id="content-search">
		<%--the form to filter news --%>
		<form method="POST" action='<c:url value="/admin/filter" />'>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div id="content-authors">
				<select id="ss" name="authorId"><option></option>
					<c:forEach items="${authorList}" var="author">
						<c:choose>
							<%-- checks if gotten author'id equals existing author'id and saves chosen author--%>
							<c:when
								test="${ctg:containsAuthor(author, searchCriteria.authorId) }">
								<option selected="selected" value="${author.authorId }">${ctg:escapeHtml4(author.authorName) }</option>
							</c:when>
							<c:otherwise>
								<%--escapes and Strings to protect XSS (Cross-Site Scripting) --%>
								<option value="${author.authorId }">${ctg:escapeHtml4(author.authorName) }</option>
							</c:otherwise>
						</c:choose>
					</c:forEach></select>
			</div>
			<div id="content-tags">
				<select id="ms" name="tags" multiple="multiple"><c:forEach
						items="${tagList}" var="tag">
						<c:choose>
							<%-- checks if a tag list consist existing tag(-s) and saves chosen tag(-s)--%>
							<c:when test="${ctg:containsTag(tag, searchCriteria.tagsId) }">
								<option selected="selected" value="${tag.tagId }">${ctg:escapeHtml4(tag.tagName) }</option>
							</c:when>
							<c:otherwise>
								<option value="${tag.tagId }">${ctg:escapeHtml4(tag.tagName) }</option>
							</c:otherwise>
						</c:choose>
					</c:forEach></select>
			</div>
			<script>
				$('#ms').multipleSelect();
			</script>
			<div id="content-filter-button">
				<input type="submit" value="<fmt:message key="search.filter" />" />
			</div>
		</form>
		<%--resets the filtering settings --%>
		<div id="content-reset-button">
			<form method="POST" action='<c:url value="/admin/reset" />'>
				<%--this field is used to protect against CSRF (Cross Site Request Forgery)--%>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" /> <input type="submit"
					value="<fmt:message key="search.reset" />">
			</form>
		</div>
	</div>
	<div id="news-list-content">
		<form method="POST" action='<c:url value="/admin/news/deletenews" />'>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<%-- cycles by a news list --%>
			<c:forEach items="${newsList}" var="news">
				<div class="short-news">
					<%-- displays titles of all news --%>
					<div class="short-news-title">
						<strong>${ctg:escapeHtml4(news.news.title) }</strong>
					</div>
					<%-- displays authors of all news --%>
					<div class="short-news-author">
						(
						<fmt:message key="news.author" />
						${ctg:escapeHtml4(news.author.authorName) })
					</div>
					<%-- displays modification dates of all news --%>
					<div class="short-news-date">
						<fmt:formatDate pattern="dd-MM-yyy "
							value="${news.news.modificationDate}" />
					</div>
					<div style="clear: both"></div>
					<%-- displays short texts of all news --%>
					<div class="short-news-text">${ctg:escapeHtml4(news.news.shortText) }</div>
					<div class="short-news-view-link">
						<%--  opens a requested news
					 news http://localhost:8029/news-admin/admin/news/143
					--%>
						<a href="news/${news.news.newsId }"><fmt:message
								key="view.news.link" /></a>
					</div>
					<%-- displays a quantity of comments --%>
					<div class="short-news-comments-number">
						<fmt:message key="comments.count" />
						(${news.comments.size() })
					</div>
					<div class="short-news-tag-list">
						<c:forEach items="${news.tags}" var="tag">
					${ctg:escapeHtml4(tag.tagName) }
				</c:forEach>
					</div>
					<div style="clear: both"></div>
				</div>
				<!-- displays the checkbox to delete news -->
				<div class="short-news-delete-checkbox">
					<%-- newsIdList is a @RequestParam for the method of deleting news --%>
					<input type="checkbox" name="newsIdList"
						value="${news.news.newsId }">
				</div>
				<div class="short-news-edit-link">
					<a
						href="<c:url value="/admin/view/editnews/${news.news.newsId }" />"><fmt:message
							key="news.edit.link" /></a>
				</div>
				<div style="clear: both"></div>
			</c:forEach>
			<!-- delete button -->
			<input id="delete-news-button" name="submit" type="submit"
				value="<fmt:message key="newslist.delete" />" />
			<div style="clear: right"></div>
		</form>
	</div>
	<div id="navigation-by-list">
		<ctg:pagination currentPage="${currentPage }"
			pagesCount="${pagesCount }" />
	</div>
</div>
<div style="clear: both"></div>