<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<script type="text/javascript">
	tagError = '<fmt:message key="tagname.error.pattern" />';
</script>
<div id="content">
	<div id="tag-content">
		<c:forEach items="${tagList}" var="tag">
			<c:choose>
			<%--  displays if it is chosen a tag for editing. 		
			 Gets the tag object to edit (editTag) from TagController  --%>
				<c:when test="${not empty editTag and editTag.tagId eq tag.tagId}">
				<%-- editTag attribute for the method of TagController --%>
					<sf:form modelAttribute="editTag" action="?edit"
						onsubmit="return validateTag()">
						<sf:hidden path="tagId" />
						<div class="edittag-tag-text">
							<fmt:message key="edittag.tag.text" />
						</div>
						<div class="edittag-inputname">
							<sf:input path="tagName" />
						</div>
						<div class="edittag-update-button">
							<input type="submit"
								value="<fmt:message key="edittag.update.button" />">
						</div>
					</sf:form>
					<%-- displays the delete link --%>
					<div class="edittag-delete-link">
						<form action="?tagId=${tag.tagId }&delete" method="POST">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <input type="submit"
								value="<fmt:message
									key="edittag.delete.link" />">
						</form>
					</div>
					<%-- displays the cancel link --%>
					<div class="edittag-cancel-link">
						<a href="?"><fmt:message key="edittag.cancel.link" /></a>
					</div>
				</c:when>
				<c:otherwise>
					<div class="edittag-tag-text">
						<fmt:message key="edittag.tag.text" />
					</div>
					<%-- displays  all tags --%>
					<div class="edittag-inputname-blocked">
						<input type="text" value="${ctg:escapeHtml4(tag.tagName) }"
							disabled="disabled" />
					</div>
					<%-- the link to chose a tag will be edited. "editTagId" is a 
					parameter for viewEditTagsPage method  --%>
					<div class="edittag-edit-link">
						<a href="?editTagId=${tag.tagId }"><fmt:message
								key="edittag.edit.link" /></a>
					</div>
				</c:otherwise>
			</c:choose>
			<div style="clear: both;"></div>
		</c:forEach>
	</div>
	<%-- displays the field to save a tag by name --%>
	<c:if test="${empty editTag or empty editTag.tagId }">
	<%-- gets from viewEditTagsPage method --%> 
		<sf:form modelAttribute="saveTag" action="?save"
			onsubmit="return validateTag()">
			<div id="edittag-addtag-text">
				<fmt:message key="edittag.addtag.text" />
			</div>
			<div id="edittag-inputname-save">
				<sf:input path="tagName" />
			</div>
			<div id="edittag-save-button">
				<input type="submit"
					value="<fmt:message key="edittag.save.button" />">
			</div>
		</sf:form>
		<div style="clear: both;"></div>
	</c:if>
	<%-- displays information about all validation errors --%>
	<div class="addnews-error-message" id="edittag-message">
	<%-- gets validationMessages object from TagController --%>
		<c:forEach items="${validationMessages}" var="error">
				${error.getDefaultMessage() }
			</c:forEach>
	</div>
</div>
<div style="clear: both"></div>