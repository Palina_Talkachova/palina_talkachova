<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div id="login-content">
	<div id="login-form">
		<c:if test="${not empty error}">
			<div id="login-error">
				<fmt:message key="${error}" />
			</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div id="login-message">
				<fmt:message key="${msg}" />
			</div>
		</c:if>
		<%--the path to the authentication filter. The form will be sent to this path.
		Spring security handles authentication requests by j_spring_security_check address --%>
		<c:url value="/j_spring_security_check" var="loginUrl" />
		<form name='loginForm' action="${loginUrl }" method='POST'>
			<table>
				<tr>
					<td><fmt:message key="login.user" /></td>
					<%-- the field for inputting a login --%>
					<td><input type='text' name='login'></td>
				</tr>
				<tr>
					<td><fmt:message key="login.password" /></td>
					<td><input type='password' name='password' /></td>
				</tr>
				<tr>
					<td colspan='2'><input name="submit" type="submit"
						value="<fmt:message key="login.enter" />" /></td>
				</tr>
			</table>
			<%--this field is used to protect against CSRF (Cross Site Request Forgery)--%>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>
</div>
<div style="clear: both"></div>