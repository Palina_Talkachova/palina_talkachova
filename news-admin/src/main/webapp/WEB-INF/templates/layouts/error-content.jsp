<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div id="content">
	<c:if test="${not empty error}">
		<div id="error-content">
			<fmt:message key="${error}" />
		</div>
	</c:if>
</div>
<div style="clear: both"></div>