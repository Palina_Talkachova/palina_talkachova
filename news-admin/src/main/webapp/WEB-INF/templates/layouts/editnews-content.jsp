<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript">
	titleError = '<fmt:message key="title.error.pattern" />';
	shortTextError = '<fmt:message key="shorttext.error.pattern" />';
	fullTextError = '<fmt:message key="fulltext.error.pattern" />';
	dateError = '<fmt:message key="date.error" />';
</script>
<div id="content">
	<%--the form to edit a news --%>
	<c:url value="/admin/view/editnews/${newsValue.news.newsId }" var="url" />
	<sf:form method="POST" modelAttribute="newsValue" action="${url }"
		id="newsForm" onsubmit="return makeValidation()">
		<sf:hidden path="news.newsId" />
		<sf:hidden path="news.creationDate" />
		<div id="addnews-title">
			<div>
				<fmt:message key="addnews.title" />
			</div>
			<div>
				<sf:input path="news.title" />
			</div>
			<div class="addnews-error-message" id="title-error">
				<sf:errors path="news.title" />
			</div>
		</div>
		<div id="addnews-date">
			<div>
				<fmt:message key="addnews.date" />
			</div>
			<div>
				<sf:input path="news.modificationDate" />
			</div>
			<div class="addnews-error-message" id="date-error">
				<sf:errors path="news.modificationDate" />
			</div>
		</div>
		<div id="addnews-short-text">
			<div>
				<fmt:message key="addnews.shorttext" />
			</div>
			<div>
				<sf:textarea rows="3" cols="50" path="news.shortText" />
			</div>
			<div class="addnews-error-message" id="short-text-error">
				<sf:errors path="news.shortText" />
			</div>
		</div>
		<div id="addnews-full-text">
			<div>
				<fmt:message key="addnews.fulltext" />
			</div>
			<div>
				<sf:textarea rows="5" cols="50" path="news.fullText" />
			</div>
			<div class="addnews-error-message" id="full-text-error">
				<sf:errors path="news.fullText" />
			</div>
		</div>
		<div id="search">
			<div id="authors">
				<sf:select id="ss" items="${authorList}" itemLabel="authorName"
					itemValue="authorId" path="author.authorId"></sf:select>
			</div>
			<div id="tags">
				<sf:select id="ms" multiple="multiple" items="${tagList}"
					itemLabel="tagName" itemValue="tagId" path="tags">
				</sf:select>
			</div>
			<script>
				$('#ms').multipleSelect();
			</script>
		</div>
		<input id="save-news-button" type="submit"
			value="<fmt:message key="addnews.save" />" />
	</sf:form>
</div>
<div style="clear: both"></div>