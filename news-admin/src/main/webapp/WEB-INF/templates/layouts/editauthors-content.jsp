<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<script type="text/javascript">
	authorError = '<fmt:message key="authorname.error.pattern" />';
</script>
<div id="content">
	<div id="author-content">
		<c:forEach items="${authorList}" var="author">
			<c:choose>
			<%--  displays if it is chosen an author for editing. 		
			 Gets the author object to edit (editAuthor) from AuthorController  --%>
				<c:when test="${not empty editAuthor and editAuthor.authorId eq author.authorId}">
					 <%-- editAuthor attribute for the method of AuthorController --%>
					<sf:form modelAttribute="editAuthor" action="?edit"
						onsubmit="return validateAuthor()">
						<sf:hidden path="authorId" />
						<div class="editauthor-author-text">
							<fmt:message key="editauthor.author.text" />
						</div>
						<!-- the field to input the new author's name  -->
						<div class="editauthor-inputname">
							<sf:input path="authorName" />
						</div>
						<div class="editauthor-update-button">
							<input type="submit"
								value="<fmt:message key="editauthor.update.button" />">
						</div>
					</sf:form>
					<%-- displays the expire link --%>
					<div class="editauthor-expire-link">
						<form action="?authorId=${author.authorId }&expire" method="POST">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <input type="submit"
								value="<fmt:message
									key="editauthor.expire.link" />">
						</form>
					</div>
					 <%-- displays the cancel link --%>
					<div class="editauthor-cancel-link">
						<a href="?"><fmt:message key="editauthor.cancel.link" /></a>
					</div>
				</c:when>
				<c:otherwise>
					<div class="editauthor-author-text">
						<fmt:message key="editauthor.author.text" />
					</div>
					<%-- displays  all authors --%>
					<div class="editauthor-inputname-blocked">
						<input type="text" value="${ctg:escapeHtml4(author.authorName) }"
							disabled="disabled" />
					</div>
					<%-- the link to chose an author will be edited. "editAuthorId" is a 
					parameter for viewEditAuthorsPage method  --%>
					<div class="editauthor-edit-link">
						<a href="?editAuthorId=${author.authorId }"><fmt:message
								key="editauthor.edit.link" /></a>
					</div>
				</c:otherwise>
			</c:choose>
			<div style="clear: both;"></div>
		</c:forEach>
	</div>
	<div>
	<%-- displays the field to save an author by name --%>
		<c:if test="${empty editAuthor or empty editAuthor.authorId }">
			<div id="editauthor-addauthor-text">
				<fmt:message key="editauthor.addauthor.text" />
			</div>
			<%-- gets from viewEditAuthorsPag method --%> 
			<sf:form modelAttribute="saveAuthor" action="?save"
				onsubmit="return validateAuthor()">
				<div id="editauthor-inputname-save">
					<sf:input path="authorName" />
				</div>
				<div id="editauthor-save-button">
					<input type="submit"
						value="<fmt:message key="editauthor.save.button" />">
				</div>
			</sf:form>
			<div style="clear: both;"></div>
		</c:if>
	</div>
	<%-- displays information about all validation errors --%>
	<div class="addnews-error-message" id="editauthor-message">
		<%-- gets validationMessages object from AuthorController --%>
		<c:forEach items="${validationMessages}" var="error">
				${error.getDefaultMessage() }
			</c:forEach>
	</div>
</div>
<div style="clear: both"></div>