package com.epam.news.service;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.epam.news.entity.UserInfo;
import com.epam.news.exception.ServiceException;

/**
 * The class UserService. Use for Spring Security authorization
 * 
 * @author Palina_Talkachova
 *
 */
@Service
// UserDetailsService interface is a core interface which loads user-specific
// data.
public class UserService implements UserDetailsService {

	private static Logger LOGGER = Logger.getLogger(UserService.class);

	@Autowired
	private UserInfoService userInfoService;

	/**
	 * gets detailed about an user
	 * 
	 * @param String
	 *            username return UserDetails object
	 */
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserInfo userInfo = null;
		try {
			userInfo = userInfoService.getUserInfoByName(username);
		} catch (ServiceException e) {
			LOGGER.error(e);
		}
		/*
		 * defines a role for obtained user. Stores a String representation of
		 * an authority granted to the Authentication object
		 */
		GrantedAuthority authority = new SimpleGrantedAuthority(
				userInfo.getRole());
		/*
		 * defines an object of UserDetails class to allow checking inputed user
		 * login and password and after it authenticate an user
		 */
		UserDetails userDetails = new User(userInfo.getUsername(),
				userInfo.getPassword(),
				// returns a fixed-size list backed by the the specified array
				Arrays.asList(authority));
		return userDetails;
	}
}
