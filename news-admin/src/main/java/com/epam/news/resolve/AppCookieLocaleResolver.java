package com.epam.news.resolve;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * The Class SmartCookieLocaleResolver. Falls back to Spring's
 * {@link SessionLocaleResolver} when no cookie is found. This allows users with
 * cookies disabled to still have a custom Locale for the duration of their
 * session.
 * 
 * @author Palina_Talkachova
 *
 */
/*
 * CookieLocaleResolver is LocaleResolver implementation that uses a cookie sent
 * back to the user in case of a custom setting, with a fallback to the
 * specified default locale or the request's accept-header locale
 */
public class AppCookieLocaleResolver extends CookieLocaleResolver {
	/*
	 * LocaleResolver implementation that uses a locale attribute in the user's
	 * session in case of a custom setting, with a fallback to the specified
	 * default locale or the request's accept-header locale
	 */private SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();

	/**
	 * sets the default locale
	 * 
	 * @param defaultLocale
	 * 
	 */
	@Override
	public void setDefaultLocale(Locale defaultLocale) {
		// sets the default Locale that this resolver will return if no other
		// locale found
		sessionLocaleResolver.setDefaultLocale(defaultLocale);
	}

	/**
	 * sets a locale
	 * 
	 * @param request
	 * @param response
	 * @param locale
	 * 
	 */
	@Override
	public void setLocale(HttpServletRequest request,
			HttpServletResponse response, Locale locale) {
		// sets the current locale to the given one.
		super.setLocale(request, response, locale);
		// sets the current locale to the given one.
		sessionLocaleResolver.setLocale(request, response, locale);
	}

	/**
	 * determines the default locale
	 * 
	 * @param request
	 * @return locale
	 * 
	 */
	/*
	 * A Locale object represents a specific geographical, political, or
	 * cultural region
	 */
	@Override
	protected Locale determineDefaultLocale(HttpServletRequest request) {
		// Resolve the current locale via the given request.
		return sessionLocaleResolver.resolveLocale(request);
	}
}
