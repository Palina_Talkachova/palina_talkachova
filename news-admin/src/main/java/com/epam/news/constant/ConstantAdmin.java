package com.epam.news.constant;

/**
 * ConstantAdmin class
 * 
 * consists constants for news-admin module of the application
 * 
 * @author Palina_Talkachova
 *
 */
public class ConstantAdmin {

	public static final String ATTRIBUTE_LOCALE = "locale";
	public static final String ATTRIBUTE_NEWS = "news";
	public static final String ATTRIBUTE_NEWS_LIST = "newsList";
	public static final String ATTRIBUTE_PAGES_COUNT = "pagesCount";
	public static final String ATTRIBUTE_CURRENT_PAGE = "currentPage";
	public static final String ATTRIBUTE_AUTHOR_LIST = "authorList";
	public static final String ATTRIBUTE_TAG_LIST = "tagList";
	public static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
	public static final String ATTRIBUTE_NEWS_ID_LIST = "idList";
	public static final String ATTRIBUTE_PREV_NEWS = "prevNews";
	public static final String ATTRIBUTE_NEXT_NEWS = "nextNews";
	public static final String ATTRIBUTE_NEWS_ID = "newsId";
	public static final String ATTRIBUTE_SAVE_AUTHOR = "saveAuthor";
	public static final String ATTRIBUTE_SAVE_TAG = "saveTag";
	public static final String ATTRIBUTE_EDIT_AUTHOR = "editAuthor";
	public static final String ATTRIBUTE_EDIT_TAG = "editTag";
	public static final String ATTRIBUTE_VALIDATION_ERROR_MESSAGES = "validationMessages";
	public static final String ATTRIBUTE_ERROR = "error";
	public static final String ATTRIBUTE_MSG = "msg";
	public static final String ATTRIBUTE_SIDE_BAR_PAGE = "sideBarPage";
	public static final String ATTRIBUTE_COMMENT = "comment";
	public static final String ATTRIBUTE_NEWS_AUTHOR = "newsAuthor";
	public static final String ATTRIBUTE_NEWS_TAG_LIST = "newsTagList";
	public static final String ATTRIBUTE_NEWS_VALUE = "newsValue";
	public static final String ATTRIBUTE_TIME_ZONE_OFFSET = "timezoneOffset";

	public static final String PARAM_LOCALE = "locale";
	public static final String PARAM_NEWS_ID = "newsId";
	public static final String PARAM_NEWS_ID_LIST = "newsIdList";
	public static final String PARAM_AUTHOR_ID = "authorId";
	public static final String PARAM_TAGS = "tags";
	public static final String PARAM_COMMENT_TEXT = "commentText";
	public static final String PARAM_COMMENT_ID = "commentId";
	public static final String PARAM_PAGE = "page";
	public static final String PARAM_ERROR_MESSAGE = "error";
	public static final String PARAM_LOGOUT_MESSAGE = "logout";
	public static final String PARAM_EDIT_AUTHOR_ID = "editAuthorId";
	public static final String PARAM_EDIT_TAG_ID = "editTagId";
	public static final String PARAM_TAG_ID = "tagId";
	public static final String PARAM_EDIT = "edit";
	public static final String PARAM_SAVE = "save";
	public static final String PARAM_EXPIRE = "expire";
	public static final String PARAM_DELETE = "delete";
	public static final String PARAM_ADD = "add";

	public static final String ROLE_ADMIN = "ROLE_ADMIN";

	public static final String VIEW_EDIT_AUTHORS = "editauthors";
	public static final String VIEW_NEWS_LIST = "news-list";
	public static final String VIEW_LOGIN = "login";
	public static final String VIEW_ADD_NEWS = "addnews";
	public static final String VIEW_EDIT_NEWS = "editnews";
	public static final String VIEW_EDIT_TAGS = "edittags";
	public static final String VIEW_NEWS = "news";
	public static final String VIEW_ERROR = "error";

	public static final String COOKIE_OFFSET = "timezoneOffset";
}
