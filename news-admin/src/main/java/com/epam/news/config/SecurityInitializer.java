package com.epam.news.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Registers filters to provide security (Spring security filters)
 * 
 * @author Palina_Talkachova
 */
/*
 * AbstractSecurityWebApplicationInitializer registers DelegationFilterProxy to
 * use SpringSecurityFilterChain DelegationFilterProxy delegates filtering other
 * filters (from javax.servlet.Filter)
 */
public class SecurityInitializer extends
		AbstractSecurityWebApplicationInitializer {
}
