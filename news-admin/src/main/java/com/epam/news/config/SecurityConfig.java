package com.epam.news.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.epam.news.service.UserService;

/**
 * Spring Security configuration for administration part of the application
 * 
 * @author Palina_Talkachova
 *
 */
// Component Scan automatically loads classes marked this annotation
@Configuration
/*
 * adding this annotation to an @Configuration class with extending the
 * WebSecurityConfigurerAdapter class provide authentication
 */
@EnableWebSecurity
/*
 * enables Spring Security global method security "securedEnabled = true"
 * determines if Spring Security's Secured annotations should be enabled
 */
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	/**
	 * registers global authentication
	 * 
	 * @param auth
	 * @throws Exception
	 */
	/*
	 * AuthenticationManagerBuilder allows for easily building in memory
	 * authentication registers custom implementation of UserDetailsService
	 * registers PasswordEncoder for casting to Md5 format
	 */
	@Autowired
	public void registerGlobalAuthentication(AuthenticationManagerBuilder auth)
			throws Exception {
		// adds authentication based upon the custom UserDetailsService (name,
		// roles, password)
		auth.userDetailsService(userService).passwordEncoder(
				new Md5PasswordEncoder());
	}

	/**
	 * manages security configuration
	 * 
	 * @param http
	 * 
	 */
	@Override
	// HttpSecurity allows web security for some http requires
	protected void configure(HttpSecurity http) throws Exception {
		// defines request rules in according with its will be gotten access to
		// resource and data
		// sets the permitted URL
		http.authorizeRequests().antMatchers("/resources/**").permitAll();
		// defines a page with a login form
		http.formLogin().loginPage("/login")
		// defines an action from a login form for authentication filter
				.loginProcessingUrl("/j_spring_security_check")
				// defines URL for incorrect log in
				.failureUrl("/login?error").usernameParameter("login")
				// gives access for all users
				.passwordParameter("password").permitAll();
		// defines URL with restricted access (only for an administrator)
		http.authorizeRequests().antMatchers("/admin/**")
				.access("hasRole('ROLE_ADMIN')");
		// permits log out for all users
		http.logout().permitAll().
		// defines URL to logout
				logoutUrl("/logout")
				// defines URL for successful logout
				.logoutSuccessUrl("/login?logout").
				// does the current session invalidated
				invalidateHttpSession(true);
	}
}
