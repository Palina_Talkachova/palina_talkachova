package com.epam.news.config;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Registers DispatcherServlet for Web MVC which manages initialization Spring
 * MVC and mapping URL patterns
 * 
 * @author Palina_Talkachova
 *
 */
/*
 * AbstractAnnotationConfigDispatcherServletInitializer is a Base class for
 * org.springframework.web.WebApplicationInitializer implementations that
 * register a DispatcherServlet configured with annotated classes, e.g. Spring's
 * 
 * @Configuration classes
 */
public class WebInitializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {

	/**
	 * specifies @Configuration and/or @Component classes to be provided to the
	 * root application context
	 * 
	 * return Class[] array
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { AdminApplicationConfig.class, SecurityConfig.class };
	}

	/**
	 * specifies @Configuration and/or @Component classes to be provided to the
	 * dispatcher servlet application context
	 * 
	 * return null
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	/**
	 * specifies the servlet mapping for the DispatcherServlet
	 */
	@Override
	protected String[] getServletMappings() {
		// all requests will be catch by DispatcherServlet
		return new String[] { "/" };
	}

	/**
	 * specifies filters to add and map to the DispatcherServlet.
	 */
	@Override
	protected Filter[] getServletFilters() {
		// sets the encoding
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		return new Filter[] { characterEncodingFilter };
	}

	/**
	 * creates a DispatcherServlet with the specified WebApplicationContext
	 */
	@Override
	// WebApplicationContext is an interface to configure web application
	protected DispatcherServlet createDispatcherServlet(
			WebApplicationContext servletAppContext) {
		DispatcherServlet dispatcherServlet = super
				.createDispatcherServlet(servletAppContext);
		// sets the exception if it isn't possible to find a handler
		dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
		return dispatcherServlet;
	}

	/**
	 * configures the given ServletContext with any servlets, filters, listeners
	 * context-params and attributes necessary for initializing this web
	 * application
	 */
	@Override
	public void onStartup(ServletContext servletContext)
			throws ServletException {
		super.onStartup(servletContext);
	}
}
