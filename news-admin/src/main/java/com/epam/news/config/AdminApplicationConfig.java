package com.epam.news.config;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.epam.news.resolve.AppCookieLocaleResolver;

/**
 * Class AdminApplicationConfig
 * 
 * Consists configuration for view components
 * 
 * @author Palina_Talkachova
 *
 */
/*
 * imports Spring MVC configuration by adding this annotation to @Configuration
 * class
 */
@EnableWebMvc
// marks a class as the configuration class
@Configuration
// imports the configuration class
@Import({ ApplicationConfig.class })
// defines paths for components
@ComponentScan("com.epam.news.controller, com.epam.news.service")
public class AdminApplicationConfig extends WebMvcConfigurerAdapter {

	/**
	 * sets the name of the resource file for locale properties and default
	 * character encoding
	 * 
	 * @return source
	 */
	/*
	 * ResourceBundleMessageSource accesses resource bundles using specified
	 * basenames
	 */
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		// sets the name of the resource file for locale properties
		source.setBasename("locale");
		// set the default charset to use for parsing resource bundle files
		source.setDefaultEncoding("UTF-8");
		return source;
	}

	/**
	 * gets a Validator
	 * 
	 * return a validator
	 */
	@Override
	public Validator getValidator() {
		// returns a validator
		return validator();
	}

	/**
	 * gets a validator with determined path for locale validation messages
	 * 
	 * @return a factory bean for local validation
	 */
	/*
	 * LocalValidatorFactoryBean is the central class for javax.validation setup
	 * in a Spring application context
	 */
	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
		/*
		 * specifies a custom Spring MessageSource for resolving validation
		 * messages
		 */
		validator.setValidationMessageSource(
		// returns the source path for locale properties
				messageSource());
		return validator;
	}

	/**
	 * Interceptor that allows for changing the current locale on every request,
	 * via a configurable request parameter (default parameter name: "locale").
	 * 
	 **/
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		return new LocaleChangeInterceptor();
	}

	/**
	 * Add Spring MVC lifecycle interceptors for pre- and post-processing of
	 * controller method invocations. Interceptors can be registered to apply to
	 * all requests or be limited to a subset of URL patterns.
	 * 
	 * @param registry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// adds an intercepter
		registry.addInterceptor(localeChangeInterceptor());
		// registry.addInterceptor(timeZoneInterceptor());
	}

	/**
	 * registers a resource handler and a resource location
	 * 
	 * @param registry
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		/*
		 * adds a resource handler for serving static resources based on the
		 * specified URL path patterns. The handler will be invoked for every
		 * incoming request that matches to one of the specified path patterns
		 */
		registry.addResourceHandler("/resources/**").
		// adds resource locations from which to serve static
		// content
				addResourceLocations("/resources/");
	}

	/**
	 * gets a view resolver for tiles
	 * 
	 * @return a view resolver for tiles
	 */
	@Bean
	/*
	 * TilesViewResolver searches a view (tiles) which determines as a logical
	 * name of view with a prefix and an ending
	 */
	public TilesViewResolver viewResolver() {
		return new TilesViewResolver();
	}

	/**
	 * gets configuration for tiles
	 * 
	 * @return tilesConfigurer
	 */
	@Bean
	/*
	 * TilesConfigurer helps to TilesViewResolver to find tiles loads tiles to
	 * make available for TilesViewResolver
	 */
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setCompleteAutoload(true);
		return tilesConfigurer;
	}

	/**
	 * determines the default locale
	 * 
	 * @return localeResolver
	 */
	@Bean
	public AppCookieLocaleResolver localeResolver() {
		AppCookieLocaleResolver localeResolver = new AppCookieLocaleResolver();
		// sets default locale
		localeResolver.setDefaultLocale(new Locale("en_US"));
		// uses for giving name for cookies created by this generator
		localeResolver.setCookieName("locale");
		return localeResolver;
	}
}
