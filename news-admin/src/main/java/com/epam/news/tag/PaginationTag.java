package com.epam.news.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * PaginationTag class
 * 
 * provides the pagination
 * 
 * @author Palina_Talkachova
 *
 */
public class PaginationTag extends TagSupport {

	private static final long serialVersionUID = -8464263595117061744L;

	// gets from jsp
	private int pagesCount;
	// gets from jsp
	private int currentPage;

	/**
	 * sets the quantity of pages
	 * 
	 * @param pagesCount
	 */
	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}

	/**
	 * sets the current page
	 * 
	 * @param currentPage
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * sets pagination
	 * 
	 * @throws JspException
	 * @return int
	 */
	@Override
	public int doStartTag() throws JspException {
		// gets access to the out stream of the servlet information have sent to
		// this stream shows a client page scope
		JspWriter out = pageContext.getOut();
		try {
			// gets information about the current page and the quantity of 
			//pages form news list jsp
			for (int i = 1; i <= pagesCount; i++) {
				if (i == currentPage) {
					// cells of the table
					out.write("<td>" + i + "</td>");
				} else {
					// sets information for news list jsp
					out.write("<td><a href=\""
							// /news-admin
							+ pageContext.getServletContext().getContextPath()
							+ "/admin/newslist?page=" + i + "\">" + i
							+ "</a></td>");
				}
			}
		} catch (IOException e) {
			throw new JspException(e);
		}
		// this constant invokes doEndTag() method
		return SKIP_BODY;
	}

	/**
	 * return int
	 */
	@Override
	public int doEndTag() throws JspException {
		// this constant allows the follow handling of a page
		return EVAL_PAGE;
	}
}
