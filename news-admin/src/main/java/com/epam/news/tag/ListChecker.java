package com.epam.news.tag;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.Tag;

/**
 * ListChecker class. Checks the equivalence of authors or tags by its id.
 * 
 * Checks the availability authors and tags
 * 
 * @author Palina_Talkachova
 *
 */
public class ListChecker {

	/**
	 * checks if gotten author'id equals existing author'id
	 * 
	 * @param author
	 * @param authorId
	 * @return boolean
	 */
	// the function-tag must be static and always have return value
	public static boolean containsAuthor(Author author, Long authorId) {
		return author.getAuthorId().equals(authorId);
	}

	/**
	 * checks if a tag list consist existing tag
	 * 
	 * @param tag
	 * @param tagIdList
	 * @return boolean
	 */
	public static boolean containsTag(Tag tag, List<Long> tagIdList) {
		for (Long tagId : tagIdList) {
			if (tag.getTagId().equals(tagId)) {
				return true;
			}
		}
		return false;
	}
}
