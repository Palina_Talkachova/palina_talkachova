package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

/**
 * Tag class. Edit a tag
 * 
 * @author Palina_Talkachova
 *
 */
@Controller
@RequestMapping(value = "/admin")
public class TagController {

	private static final String CURRENT = "tags";
	private static final String REDIRECT_PATH = "redirect:/admin/view/edittags";

	@Autowired
	private TagService tagService;

	/**
	 * prepares initial information for editing a tag
	 * 
	 * @param editTagId
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.GET)
	public ModelAndView viewEditTagsPage(
	// gets the parameter from edit tag jsp
			@RequestParam(value = PARAM_EDIT_TAG_ID, required = false) Long editTagId)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Tag> tags = tagService.takeAll();
		model.addObject(ATTRIBUTE_TAG_LIST, tags);
		// sets the name for the tag was chosen
		if (editTagId != null) {
			Tag editTag = tagService.takeById(editTagId);
			editTag.setTagName(editTag.getTagName());
			model.addObject(ATTRIBUTE_EDIT_TAG, editTag);
		}
		model.addObject(ATTRIBUTE_SAVE_TAG, new Tag());
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_EDIT_TAGS);
		return model;
	}

	/**
	 * updates a tag
	 * 
	 * @param tag
	 * @param bindingResult
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.POST, params = PARAM_EDIT)
	public ModelAndView editTag(
			@Valid @ModelAttribute(ATTRIBUTE_EDIT_TAG) Tag tag,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			model.addObject(ATTRIBUTE_EDIT_TAG, tag);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES,
					bindingResult.getFieldErrors());
			List<Tag> tags = tagService.takeAll();
			model.addObject(ATTRIBUTE_TAG_LIST, tags);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_TAGS);
			return model;
		} else {
			tagService.update(tag);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * saves a tag
	 * 
	 * @param tag
	 * @param bindingResult
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.POST, params = PARAM_SAVE)
	public ModelAndView saveTag(
			@Valid @ModelAttribute(ATTRIBUTE_SAVE_TAG) Tag tag,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			model.addObject(ATTRIBUTE_EDIT_TAG, tag);
			// gets all errors associated with a field of Tag class
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES,
					bindingResult.getFieldErrors());
			List<Tag> tags = tagService.takeAll();
			model.addObject(ATTRIBUTE_TAG_LIST, tags);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_TAGS);
			return model;
		} else {
			tagService.add(tag);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * delete a tag
	 * 
	 * @param tagId
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", params = PARAM_DELETE, method = RequestMethod.POST)
	public ModelAndView deleteTag(
			@RequestParam(value = PARAM_TAG_ID, required = true) Long tagId)
			throws ServiceException {
		tagService.deleteById(tagId);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_PATH);
		return model;
	}
}
