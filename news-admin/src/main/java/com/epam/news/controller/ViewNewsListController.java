package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;
import static com.epam.news.constant.ConstantCommon.NEWS_PER_PAGE_KEY;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.NewsVO;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsHandlingService;
import com.epam.news.service.TagService;

/**
 * 
 * ViewNewsListController class.
 * 
 * prepares information to display news
 * 
 * 
 * @author Palina_Talkachova
 *
 */
@Controller
@RequestMapping(value = "/admin")
public class ViewNewsListController {

	private static final String CURRENT = "newsList";

	@Autowired
	private NewsHandlingService service;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private Environment env;

	/**
	 * prepares information to display news
	 * 
	 * @param page
	 * @param session
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	// comes from HomeController
	@RequestMapping(value = "/newslist")
	public ModelAndView viewNewsList(
			@RequestParam(value = PARAM_PAGE, required = false) Integer page,
			HttpSession session) throws ServiceException {
		ModelAndView model = new ModelAndView();
		SearchCriteria searchCriteria = session
				.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null ? (SearchCriteria) session
				.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();
		if (page != null) {
			// by default page is 1
			searchCriteria.setPage(page);
		}
		List<NewsVO> newsList = service.findBySearchCriteria(searchCriteria);
		model.addObject(ATTRIBUTE_NEWS_LIST, newsList);
		// gets a value of the quantity of news per a page
		int newsPerPage = Integer.parseInt(env.getProperty(NEWS_PER_PAGE_KEY));
		// gets the quantity of pages for displaying all news
		long pagesCount = service.countNewsBySearchCriteria(searchCriteria)
				/ newsPerPage;
		if ((service.countNewsBySearchCriteria(searchCriteria) % newsPerPage) != 0) {
			pagesCount++;
		}
		session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		model.addObject(ATTRIBUTE_PAGES_COUNT, pagesCount);
		// by default page is 1 (the current page)
		model.addObject(ATTRIBUTE_CURRENT_PAGE, searchCriteria.getPage());
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authorService.takeAll());
		model.addObject(ATTRIBUTE_TAG_LIST, tagService.takeAll());
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_NEWS_LIST);
		return model;
	}
}
