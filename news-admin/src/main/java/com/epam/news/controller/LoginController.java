package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * sends to the login page
 * 
 * @author Palina_Talkachova
 *
 */
@Controller
public class LoginController {

	private static final String ERROR_KEY = "login.error.message";
	private static final String MSG_KEY = "login.msg.message";

	/**
	 * sends to the login page in case of inputed incorrect login data and after
	 * log outing
	 * 
	 * @param error
	 * @param logout
	 * @return model
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = PARAM_ERROR_MESSAGE, required = false) String error,
			@RequestParam(value = PARAM_LOGOUT_MESSAGE, required = false) String logout) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			// invalid data for log in
			model.addObject(ATTRIBUTE_ERROR, ERROR_KEY);
		}
		if (logout != null) {
			// log out successfully
			model.addObject(ATTRIBUTE_MSG, MSG_KEY);
		}
		// send to the login page
		model.setViewName(VIEW_LOGIN);
		return model;
	}
}
