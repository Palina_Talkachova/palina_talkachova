package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.ATTRIBUTE_ERROR;
import static com.epam.news.constant.ConstantAdmin.VIEW_ERROR;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * The global exception handler Handler will catch exception which aren't
 * indicated and handled in Controller class
 * 
 * @author Palina_Talkachova
 *
 */
/*
 * this annotation indicates the annotated class assists a "Controller". Serves
 * as a specialization of @Component, allowing for implementation classes to be
 * autodetected through classpath scanning. Usually is used for detecting
 * @ExceptionHandler methods. By default advices all controllers. Works during
 * runtime
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private static Logger LOGGER = Logger
			.getLogger(GlobalExceptionHandler.class);

	private static final String ERROR_MESSAGE_DEFAULT = "error.message.default";

	/**
	 * The handler catches any exceptions
	 * 
	 * @param e
	 * @return ModelAndView object
	 * @throws Exception
	 */
	// this annotation handles exceptions in specific handler classes and/or
	// handler methods
	@ExceptionHandler(value = Exception.class)
	public ModelAndView defaultExceptionHandler(Exception e) throws Exception {
		LOGGER.error(e);
		e.printStackTrace();
		/*
		 * If the exception is annotated with @ResponseStatus the framework
		 * handles it AnnotationUtils is a Spring Framework utility class.
		 * AnnotationUtils consists general utility methods for working with
		 * annotations
		 */
		if (AnnotationUtils.
		/*
		 * finds a single Annotation of annotationType on the supplied Class,
		 * traversing its interfaces, annotations, and superclasses if the
		 * annotation is not directly present on the given class itself
		 */
		findAnnotation(e.getClass(),
		/*
		 * marks a method or exception class with the status code and reason
		 * that should be returned
		 * 
		 * @ResponseStatus defined HTTP status code which must be set in the
		 * response sending to a client (204, 404, etc.)
		 */
		ResponseStatus.class) != null) {
			throw e;
		}
		ModelAndView model = new ModelAndView();
		// send to error page and displays default information about an error
		model.addObject(ATTRIBUTE_ERROR, ERROR_MESSAGE_DEFAULT);
		model.setViewName(VIEW_ERROR);
		return model;
	}
}