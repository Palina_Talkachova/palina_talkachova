package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Comment;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsHandlingService;

/**
 * The Class CommentController.
 * 
 * adds and deletes comments
 */
@Controller
@RequestMapping(value = "/admin")
public class CommentController {

	private static final String REDIRECT_PATH = "redirect:/admin/news/";

	@Autowired
	private NewsHandlingService newsManagementService;

	@Autowired
	private CommentService commentService;

	/**
	 * adds a comment
	 * 
	 * @param comment
	 * @param bindingResult
	 * @param session
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	// gets a parameter from news-content.jsp
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.POST, params = PARAM_ADD)
	public ModelAndView addComment(@Valid Comment comment,
			BindingResult bindingResult, HttpSession session)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			// prepares data to view the current news
			SearchCriteria searchCriteria = session
					.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null ? (SearchCriteria) session
					.getAttribute(ATTRIBUTE_SEARCH_CRITERIA)
					: new SearchCriteria();
			model.addObject(ATTRIBUTE_NEWS, newsManagementService
					.takeSingleNewsByNewsId(comment.getNewsId()));
			model.addObject(ATTRIBUTE_PREV_NEWS, newsManagementService
					.findPrevNews(comment.getNewsId(), searchCriteria));
			model.addObject(ATTRIBUTE_NEXT_NEWS, newsManagementService
					.findNextNews(comment.getNewsId(), searchCriteria));
			model.setViewName(VIEW_NEWS);
			return model;
		}
		comment.setCommentText(comment.getCommentText());
		commentService.add(comment);
		model.setViewName(REDIRECT_PATH + comment.getNewsId());
		return model;
	}

	/**
	 * deletes a comment
	 * 
	 * @param commentId
	 * @param newsId
	 * @param session
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.POST, params = PARAM_DELETE)
	public ModelAndView deleteComment(
			@RequestParam(value = PARAM_COMMENT_ID, required = true) Long commentId,
			// gets parameters from news-content.jsp
			@RequestParam(value = PARAM_NEWS_ID, required = true) Long newsId,
			HttpSession session) throws ServiceException {
		commentService.deleteById(commentId);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_PATH + newsId);
		return model;
	}
}
