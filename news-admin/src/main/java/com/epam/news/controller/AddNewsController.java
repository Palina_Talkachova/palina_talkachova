package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsHandlingService;
import com.epam.news.service.TagService;

/**
 * AddNewsController class
 * 
 * @author Palina_Talkachova
 *
 */
@Controller
// The root path of URL is managed by this controller
@RequestMapping(value = "/admin")
public class AddNewsController {

	private static final String CURRENT = "addNews";
	private static final String REDIRECT_NEWS_PATH = "redirect:/admin/news/";
	private static final String REDIRECT_NEWS_LIST_PATH = "redirect:/admin/newslist";

	@Autowired
	private NewsHandlingService newsHandlingService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	/**
	 * prepares initial information for adding a news
	 * 
	 * @return model
	 * @throws ServiceException
	 */
	// describes security attributes for business-methods
	@Secured({ ROLE_ADMIN })
	// DispatcherServlet decides which controller use depend on the path (URL)
	// comes from the link on the sidebar
	@RequestMapping(value = "/view/addnews", method = RequestMethod.GET)
	public ModelAndView viewAddNewsPage() throws ServiceException {
		ModelAndView model = new ModelAndView();
		model.addObject(ATTRIBUTE_AUTHOR_LIST,
				authorService.takeAuthorsExceptExpired());
		model.addObject(ATTRIBUTE_TAG_LIST, tagService.takeAll());
		News news = new News();
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		// news.setModificationDate(news.getCreationDate());
		NewsVO newsValue = new NewsVO();
		newsValue.setNews(news);
		model.addObject(ATTRIBUTE_NEWS_VALUE, newsValue);
		// sets the bold style of the current menu
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		// sets the name of the view (page)
		model.setViewName(VIEW_ADD_NEWS);
		return model;
	}

	/**
	 * adds a news
	 * 
	 * @param newsValue
	 * @param bindingResult
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/addnews", method = RequestMethod.POST)
	public ModelAndView addNews(
	// this annotation shows that the object must be checked before sending
	// protects against invalid input data
			@Valid// model attribute (NewsVO) is gotten from jsp
			@ModelAttribute(value = ATTRIBUTE_NEWS_VALUE) NewsVO newsValue,
			// BindingResult helps to find mistakes in forms on jsp
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		// if inputed data is incorrect will be saved
		// information about inputed tags and the current page
		if (bindingResult.hasErrors()) {
			if (newsValue.getTags() != null) {
				// sets selected tags by the user
				newsValue.setTags(tagService.takeTagListByTagIdList(newsValue
						.getTags()));
			}
			// prepares initial information for adding a news
			model.addObject(ATTRIBUTE_AUTHOR_LIST,
					authorService.takeAuthorsExceptExpired());
			model.addObject(ATTRIBUTE_TAG_LIST, tagService.takeAll());
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_ADD_NEWS);
			return model;
		}
		// saves a news
		newsValue.getNews().setModificationDate(
				newsValue.getNews().getCreationDate());
		newsValue.getNews().setTitle(newsValue.getNews().getTitle());
		newsValue.getNews().setShortText(newsValue.getNews().getShortText());
		newsValue.getNews().setFullText(newsValue.getNews().getFullText());
		newsHandlingService.saveNews(newsValue);
		// displays added news
		model.setViewName(REDIRECT_NEWS_PATH + newsValue.getNews().getNewsId());
		return model;
	}

	/**
	 * deletes a news
	 * 
	 * @param newsIdArray
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/deletenews")
	public ModelAndView deleteNews(
			/*
			 * this annotation is used for binding request parameters with
			 * methods parameters when its names are different. Gets the request
			 * parameter (newsIdList) from news-list jsp. "required = false"
			 * means that the parameter isn't requires (by default is true)
			 */
			@RequestParam(value = PARAM_NEWS_ID_LIST, required = false) Long[] newsIdArray)
			throws ServiceException {
		List<Long> newsIdList = new ArrayList<Long>();
		if (newsIdArray != null && newsIdArray.length != 0) {
			newsIdList = Arrays.asList(newsIdArray);
		}
		newsHandlingService.deleteListOfNews(newsIdList);
		ModelAndView model = new ModelAndView();
		// redirects after the request
		model.setViewName(REDIRECT_NEWS_LIST_PATH);
		return model;
	}

	/**
	 * registers the custom editor converts id of chosen tags by an user to Long
	 * type
	 * 
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		// registers the given custom property editor for the given type and
		// property
		binder.registerCustomEditor(
		// gets tags from add-news jsp
				List.class, "tags",
				// creates a new CustomCollectionEditor for the given target
				// type
				new CustomCollectionEditor(List.class) {
					// converts each encountered Collection/array element
					@Override
					protected Object convertElement(Object element) {
						// id of tags
						if (element instanceof String) {
							Tag tag = new Tag();
							tag.setTagId(Long.parseLong((String) element));
							return tag;
						} else {
							return null;
						}
					}
				});
	}
}
