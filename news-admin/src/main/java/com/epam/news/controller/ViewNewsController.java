package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.ATTRIBUTE_COMMENT;
import static com.epam.news.constant.ConstantAdmin.ATTRIBUTE_NEWS;
import static com.epam.news.constant.ConstantAdmin.ATTRIBUTE_NEXT_NEWS;
import static com.epam.news.constant.ConstantAdmin.ATTRIBUTE_PREV_NEWS;
import static com.epam.news.constant.ConstantAdmin.ATTRIBUTE_SEARCH_CRITERIA;
import static com.epam.news.constant.ConstantAdmin.ROLE_ADMIN;
import static com.epam.news.constant.ConstantAdmin.VIEW_NEWS;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Comment;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsHandlingService;

/**
 * 
 * ViewNewsController class.
 * 
 * @author Palina_Talkachova
 *
 */
@Controller
@RequestMapping(value = "/admin")
public class ViewNewsController {
	@Autowired
	private NewsHandlingService service;

	/**
	 * displays a news content
	 * 
	 * @param newsId
	 * @param session
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}")
	// gets a path variable from URL ("/news/{newsId}")
	public ModelAndView viewNews(@PathVariable(value = "newsId") Long newsId,
			HttpSession session) throws ServiceException {
		NewsVO news = service.takeSingleNewsByNewsId(newsId);
		ModelAndView model = new ModelAndView();
		SearchCriteria searchCriteria = session
				.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null ? (SearchCriteria) session
				.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();
		model.addObject(ATTRIBUTE_NEWS, news);
		model.addObject(ATTRIBUTE_PREV_NEWS,
				service.findPrevNews(newsId, searchCriteria));
		model.addObject(ATTRIBUTE_NEXT_NEWS,
				service.findNextNews(newsId, searchCriteria));
		Comment comment = new Comment();
		comment.setCreationDate(new Date());
		comment.setNewsId(newsId);
		model.addObject(ATTRIBUTE_COMMENT, comment);
		model.setViewName(VIEW_NEWS);
		return model;
	}
}
