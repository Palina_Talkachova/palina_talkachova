package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.*;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsHandlingService;
import com.epam.news.service.TagService;

/**
 * The Class EditNewsController. Edits a news
 */
@Controller
@RequestMapping(value = "/admin")
public class EditNewsController {

	private static final String REDIRECT_PATH = "redirect:/admin/news/";

	@Autowired
	private NewsHandlingService service;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	/**
	 * prepares information to edit a news
	 * 
	 * @param newsId
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editnews/{newsId}", method = RequestMethod.GET)
	public ModelAndView viewEditNewsPage(
	// gets a news id from URL ("/view/editnews/{newsId}")
			@PathVariable(value = "newsId") Long newsId)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		// gets a single news
		NewsVO newsVO = service.takeSingleNewsByNewsId(newsId);
		// sets information about the news
		News news = newsVO.getNews();
		news.setTitle(news.getTitle());
		news.setShortText(news.getShortText());
		news.setFullText(news.getFullText());
		List<Author> authors = authorService.takeAuthorsExceptExpired();
		if (newsVO.getAuthor() != null
				&& newsVO.getAuthor().getExpired() != null) {
			authors.add(newsVO.getAuthor());
		}
		model.addObject(ATTRIBUTE_TAG_LIST, tagService.takeAll());
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
		model.addObject(ATTRIBUTE_NEWS_VALUE, newsVO);
		model.setViewName(VIEW_EDIT_NEWS);
		return model;
	}

	/**
	 * edits a news
	 * 
	 * @param newsValue
	 * @param bindingResult
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editnews/{newsId}", method = RequestMethod.POST)
	public ModelAndView editNews(
			@Valid @ModelAttribute(value = ATTRIBUTE_NEWS_VALUE) NewsVO newsValue,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			if (newsValue.getTags() != null) {
				newsValue.setTags(tagService.takeTagListByTagIdList(newsValue
						.getTags()));
			}
			model.addObject(ATTRIBUTE_AUTHOR_LIST,
					authorService.takeAuthorsExceptExpired());
			model.addObject(ATTRIBUTE_TAG_LIST, tagService.takeAll());
			model.setViewName(VIEW_EDIT_NEWS);
			return model;
		}
		newsValue.getNews().setTitle(newsValue.getNews().getTitle());
		newsValue.getNews().setShortText(newsValue.getNews().getShortText());
		newsValue.getNews().setFullText(newsValue.getNews().getFullText());
		service.updateNews(newsValue);
		model.setViewName(REDIRECT_PATH + newsValue.getNews().getNewsId());
		return model;
	}

	/**
	 * registers the custom editor converts id of chosen tags by an user to Long
	 * type
	 * 
	 * @param binder
	 */
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		// registers the given custom property editor for the given type and
		// property
		binder.registerCustomEditor(List.class, "tags",
		// creates a new CustomCollectionEditor for the given target
		// type
				new CustomCollectionEditor(List.class) {
					// converts each encountered Collection/array element
					@Override
					protected Object convertElement(Object element) {
						if (element instanceof String) {
							Tag tag = new Tag();
							tag.setTagId(Long.parseLong((String) element));
							return tag;
						} else {
							return null;
						}
					}
				});
	}
}
