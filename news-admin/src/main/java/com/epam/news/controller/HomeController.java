package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.VIEW_LOGIN;

import org.springframework.stereotype.Controller;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * HomeController
 * 
 * Sends the user to view news page or logging page
 * 
 * @author Palina_Talkachova
 *
 */
@Controller
public class HomeController {

	private static final String REDIRECT_PATH = "redirect:/admin/newslist";

	/**
	 * sends the user to view news page or logging page depends on passing
	 * authentication
	 * 
	 * @return model
	 */

	// uses for all requests
	@RequestMapping(value = "/")
	public ModelAndView home() {
		/*
		 * Holder for both Model (Map) and View (String) in the web MVC
		 * framework. Sends to DispatcherServlet view and model
		 */
		ModelAndView model = new ModelAndView();
		/*
		 * Authentication represents an interface for getting or setting
		 * authentication SecurityContextHolder consists information about the
		 * current security context, (detailed user information which works with
		 * application)
		 */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		/*
		 * AnonymousAuthenticationToken represents an anonymous Authentication
		 * (the proccesure of legality verification)
		 */
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setViewName(REDIRECT_PATH);
		} else {
			model.setViewName(VIEW_LOGIN);
		}
		return model;
	}
}
