package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;

/**
 * The Class AuthorController
 * 
 * Adds, edits and expires authors
 */
@Controller
@RequestMapping(value = "/admin")
public class AuthorController {

	private static final String CURRENT = "authors";
	private static final String REDIRECT_PATH = "redirect:/admin/view/editauthors";

	@Autowired
	private AuthorService authorService;

	/**
	 * prepares initial information for editing a news
	 * 
	 * @param editAuthorId
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", method = RequestMethod.GET)
	public ModelAndView viewEditAuthorsPage(
	// gets the parameter from edit author jsp
			@RequestParam(value = PARAM_EDIT_AUTHOR_ID, required = false) Long editAuthorId)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authors = authorService.takeAuthorsExceptExpired();
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
		// sets the name for the author was chosen
		if (editAuthorId != null) {
			Author editAuthor = authorService.takeById(editAuthorId);
			editAuthor.setAuthorName(editAuthor.getAuthorName());
			model.addObject(ATTRIBUTE_EDIT_AUTHOR, editAuthor);
		}
		model.addObject(ATTRIBUTE_SAVE_AUTHOR, new Author());
		// sets the bold style of the current menu
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_EDIT_AUTHORS);
		return model;
	}

	/**
	 * updates an author
	 * 
	 * @param author
	 * @param bindingResult
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", method = RequestMethod.POST, params = PARAM_EDIT)
	public ModelAndView editAuthor(
			@Valid// gets a model attribute from jsp
			@ModelAttribute(ATTRIBUTE_EDIT_AUTHOR) Author author,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			model.addObject(ATTRIBUTE_EDIT_AUTHOR, author);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES,
			// gets all errors associated with a field of Author class
					bindingResult.getFieldErrors());
			List<Author> authors = authorService.takeAuthorsExceptExpired();
			model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_AUTHORS);
			return model;
		} else {
			// updates the author by inputed information
			authorService.update(author);
			// redirects after the request
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * saves an author
	 * 
	 * @param author
	 * @param bindingResult
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", method = RequestMethod.POST, params = PARAM_SAVE)
	public ModelAndView saveAuthor(
			@Valid @ModelAttribute(ATTRIBUTE_SAVE_AUTHOR) Author author,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			// if inputed data is incorrect will be saved
			// information about editing author and the current page
			model.addObject(ATTRIBUTE_EDIT_AUTHOR, author);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES,
					bindingResult.getFieldErrors());
			List<Author> authors = authorService.takeAuthorsExceptExpired();
			model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_AUTHORS);
			return model;
		} else {
			authorService.add(author);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * expires an author
	 * 
	 * @param authorId
	 * @return model
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", params = PARAM_EXPIRE, method = RequestMethod.POST)
	public ModelAndView expireAuthor(
	/*
	 * this annotation is used for binding request parameters with methods
	 * parameters when its names are different
	 */
	@RequestParam(value = PARAM_AUTHOR_ID, required = true) Long authorId)
			throws ServiceException {
		Author author = authorService.takeById(authorId);
		author.setAuthorName(author.getAuthorName());
		authorService.expireAuthor(author);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_PATH);
		return model;
	}
}
