package com.epam.news.controller;

import static com.epam.news.constant.ConstantAdmin.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;

@Controller
@RequestMapping(value = "/admin")
public class FilterController {

	private static final String REDIRECT_PATH = "redirect:/admin/newslist";

	/**
	 * prepares information to display news by a search criteria
	 * 
	 * @param authorId
	 * @param tagIdArray
	 * @param session
	 * @return ModelAndView
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	// comes from news list content jsp
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	public ModelAndView filter(
			// gets from filter form
			@RequestParam(value = PARAM_AUTHOR_ID, required = false) Long authorId,
			@RequestParam(value = PARAM_TAGS, required = false) Long[] tagIdArray,
			HttpSession session) throws ServiceException {
		List<Long> tagList = new ArrayList<Long>();
		if (tagIdArray != null && tagIdArray.length != 0) {
			tagList = Arrays.asList(tagIdArray);
		}
		// displays news by the search criteria
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagsId(tagList);
		session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		return new ModelAndView(REDIRECT_PATH);
	}

	/**
	 * resets a search criteria attribute to display all news
	 * 
	 * @param session
	 * @return ModelAndView
	 * @throws ServiceException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public ModelAndView reset(HttpSession session) throws ServiceException {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!! FilterController 2 ");
		session.removeAttribute(ATTRIBUTE_SEARCH_CRITERIA);
		// sends to the page with all news
		return new ModelAndView(REDIRECT_PATH);
	}
}
