<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:setBundle basename="locale" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div id="search">
		<form method="POST" action="Controller">
			<input name="action" type="hidden" value="filter" />
			<div id="authors">
				<select id="ss" name="authorId"><option></option>
					<c:forEach items="${authorList}" var="author">
						<c:choose>
							<c:when
								test="${ctg:containsAuthor(author, searchCriteria.authorId) }">
								<option selected="selected" value="${author.authorId }">${ctg:escapeHtml4(author.authorName) }</option>
							</c:when>
							<c:otherwise>
								<option value="${author.authorId }">${ctg:escapeHtml4(author.authorName) }</option>
							</c:otherwise>
						</c:choose>
					</c:forEach></select>
			</div>
			<div id="tags">
				<select id="ms" name="tags" multiple="multiple"><c:forEach
						items="${tagList}" var="tag">
						<c:choose>
							<c:when test="${ctg:containsTag(tag, searchCriteria.tagsId) }">
								<option selected="selected" value="${tag.tagId }">${ctg:escapeHtml4(tag.tagName) }</option>
							</c:when>
							<c:otherwise>
								<option value="${tag.tagId }">${ctg:escapeHtml4(tag.tagName) }</option>
							</c:otherwise>
						</c:choose>
					</c:forEach></select>
			</div>
			<script>
				$('#ms').multipleSelect();
			</script>
			<div id="filter">
				<input type="submit" value="<fmt:message key="search.filter" />" />
			</div>
		</form>
		<div id="reset">
			<form method="POST" action="Controller">
				<input name="action" type="hidden" value="reset" /> <input
					type="submit" value="<fmt:message key="search.reset" />">
			</form>
		</div>
	</div>
	<div id="news-list-content">
		<c:forEach items="${newsList}" var="news">
			<div class="news">
				<div class="news-title">
					<strong>${ctg:escapeHtml4(news.news.title) }</strong>
				</div>
				<div class="news-author">
					(
					<fmt:message key="news.author" />
					${ctg:escapeHtml4(news.author.authorName) })
				</div>
				<div class="news-date">
					<fmt:formatDate pattern="dd-MM-yyy "
						value="${news.news.modificationDate}" />
				</div>
				<div style="clear: right"></div>
				<div class="news-short-text">${ctg:escapeHtml4(news.news.shortText) }</div>
				<div class="view-link">
					<a href="Controller?action=viewNews&newsId=${news.news.newsId }"><fmt:message
							key="view.news.link" /></a>
				</div>
				<div class="news-comments">
					<fmt:message key="comments.count" />
					(${news.comments.size() })
				</div>
				<div class="tag-list">
					<c:forEach items="${news.tags}" var="tag">
					${ctg:escapeHtml4(tag.tagName) }
				</c:forEach>
				</div>
				<div style="clear: right"></div>
			</div>
		</c:forEach>
	</div>
	<center>
		<div id="navigation-by-list\">
			<ctg:pagination currentPage="${currentPage }"
				pagesCount="${pagesCount }" />
		</div>
	</center>
</body>
</html>