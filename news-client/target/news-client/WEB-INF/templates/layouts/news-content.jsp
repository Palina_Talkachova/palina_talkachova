<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:setBundle basename="locale" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div id="news-content">
		<div id="backButton">
			<a href="Controller?action=viewNewsList"><fmt:message
					key="back.button" /></a>
		</div>
		<div class="news">
			<div class="news-title">
				<strong>${news.news.title }</strong>
			</div>
			<div class="news-author">(by
				${ctg:escapeHtml4(news.author.authorName) })</div>
			<div class="news-date">
				<fmt:formatDate pattern="dd-MM-yyy "
					value="${news.news.modificationDate}" />
			</div>
			<div style="clear: right"></div>
			<div id="news-full-text">${ctg:escapeHtml4(news.news.fullText) }</div>
		</div>
		<div id="comments">
			<c:forEach items="${news.comments}" var="comment">
				<div class="comment">
					<div class="comment-date">
						<fmt:formatDate pattern="dd-MM-yyy HH:mm:ss"
							value="${comment.creationDate}" />
					</div>
				</div>
				<div class="comment-text">${ctg:escapeHtml4(comment.commentText) }</div>
			</c:forEach>
			<form method="POST" action="Controller">
				<div>
					<input name="action" type="hidden" value="addComment" /> <input
						name="newsId" type="hidden" value="${news.news.newsId }" />
				</div>
				<div id="write-comment">
					<textarea rows="5" cols="35" name="commentText" required></textarea>
				</div>
				<div id="post-comment-button">
					<input type="submit"
						value="<fmt:message key="post.comment.button" />" />
				</div>
			</form>
		</div>
		<div style="clear: right"></div>
		<div id="prevButton">
			<c:if test="${not empty prevNews}">
				<a href="Controller?action=viewNews&newsId=${prevNews.newsId }"><fmt:message
						key="prev.button" /></a>
			</c:if>
		</div>
		<div id="nextButton">
			<c:if test="${not empty nextNews}">
				<a href="Controller?action=viewNews&newsId=${nextNews.newsId }"><fmt:message
						key="next.button" /></a>
			</c:if>
		</div>
	</div>
</body>
</html>