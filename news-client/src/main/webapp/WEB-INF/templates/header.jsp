<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!-- binds properties file with current locale -->
<fmt:setBundle basename="locale" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="css/style.css" />
</head>
<body>
	<div id="header">
		<div id="title">
			<strong><fmt:message key="title.title" /></strong>
		</div>
		<div id="locale">
			<div id="localeEN">
				<a href="Controller?action=changeLocale&locale=en"><fmt:message
						key="title.en" /></a>
			</div>
			<div id="localeRU">
				<a href="Controller?action=changeLocale&locale=ru"><fmt:message
						key="title.ru" /></a>
			</div>
		</div>
		<div style="clear: left"></div>
	</div>
</body>
</html>