package com.epam.news.config;

import java.util.ResourceBundle;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.epam.news.config.ApplicationConfig;

/**
 * ClientApplicationConfig class.
 * 
 * Gets configuration information
 * 
 * @author Palina_Talkachova
 *
 */
@Configuration
@ComponentScan("com.epam.news.action, com.epam.news.manager")
@Import(ApplicationConfig.class)
public class ClientApplicationConfig {

	private static final String PAGES_PROPERTY_FILE = "config";

	/**
	 * gets configuration information from property file
	 * 
	 * @return ResourceBundle object
	 */
	@Bean
	public ResourceBundle resourceBundle() {
		return ResourceBundle.getBundle(PAGES_PROPERTY_FILE);
	}
}