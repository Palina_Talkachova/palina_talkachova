package com.epam.news.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * EncodingFilter class
 * 
 * sets character encoding
 * 
 * @author Palina_Talkachova
 *
 */
// all requests
@WebFilter(urlPatterns = { "/*" }, initParams = {
// parameters
@WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param") })
public class EncodingFilter implements Filter {

	private String code;

	/**
	 * gets initialization parameter
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		// the value of the named initialization parameter
		code = filterConfig.getInitParameter("encoding");
	}

	/**
	 * sets character encoding
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		String codeRequest = request.getCharacterEncoding();
		if (code != null && !code.equalsIgnoreCase(codeRequest)) {
			request.setCharacterEncoding(code);
			response.setCharacterEncoding(code);
		}
		chain.doFilter(request, response);
	}

	/**
	 * destroys encoding
	 */
	public void destroy() {
		code = null;
	}
}
