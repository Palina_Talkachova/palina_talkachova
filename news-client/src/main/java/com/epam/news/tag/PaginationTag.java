package com.epam.news.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class PaginationTag extends TagSupport {

	private static final long serialVersionUID = -8464263595117061744L;

	// gets from jsp
	private int pagesCount;
	// gets from jsp
	private int currentPage;

	/**
	 * 
	 * @param pagesCount
	 */
	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}

	/**
	 * 
	 * @param currentPage
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * sets pagination
	 * 
	 * @throws JspException
	 * @return int
	 */
	@Override
	public int doStartTag() throws JspException {
		// gets access to the out stream of the servlet information have sent to
		// this stream shows a client page scope
		JspWriter out = pageContext.getOut();
		try {
			for (int i = 1; i <= pagesCount; i++) {
				if (i == currentPage) {
					// cells of the table
					out.write("<td>" + i + "</td>");
				} else {
					out.write("<td><a href=\"Controller?action=viewNewsList&page="
							+ i + "\">" + i + "</a></td>");
				}
			}
		} catch (IOException e) {
			throw new JspException(e);
		}
		return SKIP_BODY;
	}

	/**
	 * return int
	 */
	@Override
	public int doEndTag() throws JspException {
		// this constant allows the follow handling of a page
		return EVAL_PAGE;
	}
}
