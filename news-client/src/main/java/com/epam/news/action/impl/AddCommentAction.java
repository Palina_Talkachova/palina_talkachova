package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantClient.*;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.entity.Comment;
import com.epam.news.entity.NewsVO;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigPropertyManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsHandlingService;

/**
 * AddCommentAction class
 * 
 * Adds a comment
 * 
 * @author Palina_Talkachova
 *
 */
@Component(value = ACTION_ADD_COMMENT)
public class AddCommentAction implements Action {

	@Autowired
	private ConfigPropertyManager configurationManager;

	@Autowired
	private CommentService commentService;

	@Autowired
	private NewsHandlingService newsManagementService;

	/**
	 * adds a comment
	 * 
	 * @param request
	 * @return ResponsePage
	 * @throws ServiceException
	 */
	public ResponsePage execute(HttpServletRequest request)
			throws ServiceException {
		Comment comment = new Comment();
		comment.setCommentText(request.getParameter(PARAM_COMMENT_TEXT));
		comment.setCreationDate(new Date());
		Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
		comment.setNewsId(newsId);
		commentService.add(comment);
		NewsVO news = newsManagementService.takeSingleNewsByNewsId(newsId);
		request.setAttribute(ATTRIBUTE_NEWS, news);
		// sends to view-news.jsp
		return new ResponsePage(
				configurationManager.getProperty(PAGE_VIEW_NEWS_ACTION), true);
	}
}
