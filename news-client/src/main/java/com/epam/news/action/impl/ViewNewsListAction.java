package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantClient.*;
import static com.epam.news.constant.ConstantCommon.NEWS_PER_PAGE_KEY;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigPropertyManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsHandlingService;
import com.epam.news.service.TagService;

/**
 * ViewNewsListAction class
 * 
 * manages a list of news
 * 
 * @author Palina_Talkachova
 *
 */
@Component(value = ACTION_VIEW_NEWS_LIST)
public class ViewNewsListAction implements Action {

	@Autowired
	private ConfigPropertyManager configurationManager;

	@Autowired
	private NewsHandlingService newsHandlingService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private Environment env;

	/**
	 * sets information to view a list of news
	 * 
	 * @param request
	 * @return ResponsePage
	 * @throws ServiceException
	 */
	public ResponsePage execute(HttpServletRequest request)
			throws ServiceException {
		SearchCriteria attrSearchCriteria = (SearchCriteria) request
				.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
		SearchCriteria searchCriteria = attrSearchCriteria != null ? attrSearchCriteria
				: new SearchCriteria();
		String paramPage = request.getParameter(PARAM_PAGE);
		if (paramPage != null) {
			searchCriteria.setPage(Integer.parseInt(paramPage));
		}
		List<NewsVO> newsList = newsHandlingService
				.findBySearchCriteria(searchCriteria);
		request.setAttribute(ATTRIBUTE_NEWS_LIST, newsList);
		int quantityNewsOnPage = Integer.parseInt(env
				.getProperty(NEWS_PER_PAGE_KEY));
		long quantityNewsByCriteria = newsHandlingService
				.countNewsBySearchCriteria(searchCriteria);
		long quantityPage = quantityNewsByCriteria / quantityNewsOnPage;
		if ((quantityNewsByCriteria % quantityNewsOnPage) != 0) {
			quantityPage++;
		}
		request.setAttribute(ATTRIBUTE_QUANTITY_PAGES, quantityPage);
		request.setAttribute(ATTRIBUTE_CURRENT_PAGE, searchCriteria.getPage());
		request.setAttribute(ATTRIBUTE_AUTHOR_LIST, authorService.takeAll());
		request.setAttribute(ATTRIBUTE_TAG_LIST, tagService.takeAll());
		request.getSession().setAttribute(ATTRIBUTE_SEARCH_CRITERIA,
				searchCriteria);
		request.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE,
				PAGE_VIEW_NEWS_LIST_ACTION);
		return new ResponsePage(
				configurationManager.getProperty(PAGE_VIEW_NEWS_LIST), false);
	}
}
