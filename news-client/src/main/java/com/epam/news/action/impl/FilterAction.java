package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantClient.*;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigPropertyManager;
import com.epam.news.page.ResponsePage;

/**
 * manages search criteria to display news
 * 
 * @author Palina_Talkachova
 *
 */
@Component(value = ACTION_FILTER)
public class FilterAction implements Action {

	@Autowired
	private ConfigPropertyManager configurationManager;

	/**
	 * sends to index.jsp with new search criteria
	 * 
	 * @param request
	 * @return ResponsePage
	 * @throws ServiceException
	 */
	public ResponsePage execute(HttpServletRequest request)
			throws ServiceException {
		request.getSession().setAttribute(ATTRIBUTE_SEARCH_CRITERIA,
				getSearchCriteria(request));
		return new ResponsePage(configurationManager.getProperty(PAGE_INDEX),
				false);
	}

	/**
	 * sets information for search criteria
	 * 
	 * @param request
	 * @return searchCriteria
	 */
	private SearchCriteria getSearchCriteria(HttpServletRequest request) {
		Long authorId = request.getParameter(PARAM_AUTHOR_ID) != null
				&& !request.getParameter(PARAM_AUTHOR_ID).isEmpty() ? Long
				.parseLong(request.getParameter(PARAM_AUTHOR_ID)) : null;
		List<Long> tagList = new ArrayList<Long>();
		String[] tagArray = request.getParameterValues(PARAM_TAGS);
		if (tagArray != null) {
			for (String tag : tagArray) {
				tagList.add(Long.parseLong(tag));
			}
		}
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagsId(tagList);
		return searchCriteria;
	}
}
