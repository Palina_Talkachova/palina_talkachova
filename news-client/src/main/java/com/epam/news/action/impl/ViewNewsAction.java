package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantClient.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigPropertyManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.NewsHandlingService;

/**
 * ViewNewsAction class
 * 
 * Manages a single news
 * 
 * @author Palina_Talkachova
 *
 */
@Component(value = ACTION_VIEW_NEWS)
public class ViewNewsAction implements Action {

	@Autowired
	private ConfigPropertyManager configurationManager;

	@Autowired
	private NewsHandlingService service;

	/**
	 * sets information to view a single news
	 * 
	 * @param request
	 * @return ResponsePage
	 * @throws ServiceException
	 */
	public ResponsePage execute(HttpServletRequest request)
			throws ServiceException {
		long newsId = request.getParameter(PARAM_NEWS_ID) == null ? (long) request
				.getSession().getAttribute(PARAM_NEWS_ID) : Long
				.parseLong(request.getParameter(PARAM_NEWS_ID));
		SearchCriteria searchCriteria = request.getSession().getAttribute(
				ATTRIBUTE_SEARCH_CRITERIA) != null ? (SearchCriteria) request
				.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA)
				: new SearchCriteria();
		NewsVO news = service.takeSingleNewsByNewsId(newsId);
		request.setAttribute(ATTRIBUTE_NEWS, news);
		request.setAttribute(ATTRIBUTE_PREV_NEWS,
				service.findPrevNews(newsId, searchCriteria));
		request.setAttribute(ATTRIBUTE_NEXT_NEWS,
				service.findNextNews(newsId, searchCriteria));
		request.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE,
				PAGE_VIEW_NEWS_ACTION);
		request.getSession().setAttribute(ATTRIBUTE_NEWS_ID, newsId);
		return new ResponsePage(
				configurationManager.getProperty(PAGE_VIEW_NEWS), false);
	}
}
