package com.epam.news.action.impl;

import javax.servlet.http.HttpServletRequest;

import static com.epam.news.constant.ConstantClient.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigPropertyManager;
import com.epam.news.page.ResponsePage;

/**
 * ChangeLocaleAction class
 * 
 * Changes a locale
 * 
 * @author Palina_Talkachova
 *
 */
@Component(value = ACTION_CHANGE_LOCALE)
public class ChangeLocaleAction implements Action {

	@Autowired
	private ConfigPropertyManager configurationManager;

	/**
	 * changes a locale
	 * 
	 * @param request
	 * @return ResponsePage
	 * @throws ServiceException
	 */
	public ResponsePage execute(HttpServletRequest request) {
		String lang = request.getParameter(PARAM_LOCALE);
		request.getSession().setAttribute(ATTRIBUTE_LOCALE, lang);
		String page = configurationManager.getProperty((String) request
				.getSession().getAttribute(ATTRIBUTE_ACTUAL_PAGE));
		return new ResponsePage(page, false);
	}
}
