package com.epam.news.action;

import javax.servlet.http.HttpServletRequest;

import com.epam.news.exception.ServiceException;
import com.epam.news.page.ResponsePage;

/**
 * Action interface
 * 
 * @author Palina_Talkachova
 *
 */
public interface Action {

	/**
	 * execute the request
	 * 
	 * @param request
	 * @return ResponsePage
	 * @throws ServiceException
	 */
	ResponsePage execute(HttpServletRequest request) throws ServiceException;
}
