package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantClient.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigPropertyManager;
import com.epam.news.page.ResponsePage;

/**
 * ResetAction class
 * 
 * remove search criteria
 * 
 * @author Palina_Talkachova
 *
 */
@Component(value = ACTION_RESET)
public class ResetAction implements Action {

	@Autowired
	private ConfigPropertyManager configurationManager;

	/**
	 * remove search criteria
	 * 
	 * @param request
	 * @return ResponsePage
	 * @throws ServiceException
	 */
	public ResponsePage execute(HttpServletRequest request)
			throws ServiceException {
		request.getSession().removeAttribute(ATTRIBUTE_SEARCH_CRITERIA);
		return new ResponsePage(configurationManager.getProperty(PAGE_INDEX),
				false);
	}
}
