package com.epam.news.controller;

import static com.epam.news.constant.ConstantClient.ATTRIBUTE_ERROR;
import static com.epam.news.constant.ConstantClient.PARAM_ACTION;
import static com.epam.news.constant.ConstantClient.PAGE_ERROR;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.news.action.Action;
import com.epam.news.config.ClientApplicationConfig;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigPropertyManager;
import com.epam.news.page.ResponsePage;

/**
 * Controller class
 * 
 * @author Palina_Talkachova
 *
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = -4582378231457018142L;

	private static Logger LOGGER = Logger.getLogger(Controller.class);

	private ApplicationContext context;

	/**
	 * gets application context
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		context = new AnnotationConfigApplicationContext(
				ClientApplicationConfig.class);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * returns a page depend on the request
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponsePage page = null;
		// gets configuration information from property file
		// gets a property depend on a parameter
		Action action = (Action) context.getBean(request
				.getParameter(PARAM_ACTION));
		ConfigPropertyManager configurationManager = (ConfigPropertyManager) context
				.getBean(ConfigPropertyManager.class);
		try {
			// returns a page depend on the request
			page = action.execute(request);
		} catch (ServiceException e) {
			LOGGER.error(e);
			request.setAttribute(ATTRIBUTE_ERROR, e.getMessage());
			page = new ResponsePage(
					configurationManager.getProperty(PAGE_ERROR), false);
		}
		// if a flag is true (redirect)
		if (page.isRedirect()) {
			response.sendRedirect(page.toString());
			// sends forward
		} else {
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher(page.toString());
			requestDispatcher.forward(request, response);
		}
	}
}
