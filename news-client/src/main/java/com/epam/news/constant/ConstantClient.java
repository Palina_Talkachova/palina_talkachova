package com.epam.news.constant;

/**
 * ConstantClient class
 * 
 * @author Palina_Talkachova
 *
 */
public class ConstantClient {

	public static final String ACTION_CHANGE_LOCALE = "changeLocale";
	public static final String ACTION_VIEW_NEWS = "viewNews";
	public static final String ACTION_VIEW_NEWS_LIST = "viewNewsList";
	public static final String ACTION_ADD_COMMENT = "addComment";
	public static final String ACTION_FILTER = "filter";
	public static final String ACTION_RESET = "reset";

	public static final String ATTRIBUTE_LOCALE = "locale";
	public static final String ATTRIBUTE_NEWS = "news";
	public static final String ATTRIBUTE_NEWS_LIST = "newsList";
	public static final String ATTRIBUTE_QUANTITY_PAGES = "pagesCount";
	public static final String ATTRIBUTE_CURRENT_PAGE = "currentPage";
	public static final String ATTRIBUTE_AUTHOR_LIST = "authorList";
	public static final String ATTRIBUTE_TAG_LIST = "tagList";
	public static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
	public static final String ATTRIBUTE_NEWS_ID_LIST = "idList";
	public static final String ATTRIBUTE_PREV_NEWS = "prevNews";
	public static final String ATTRIBUTE_NEXT_NEWS = "nextNews";
	public static final String ATTRIBUTE_ERROR = "error";
	public static final String ATTRIBUTE_ACTUAL_PAGE = "actualPage";
	public static final String ATTRIBUTE_NEWS_ID = "newsId";
	public static final String ATTRIBUTE_TIME_ZONE_OFFSET = "timezoneOffset";

	public static final String PAGE_VIEW_NEWS = "path.page.news";
	public static final String PAGE_VIEW_NEWS_LIST = "path.page.newslist";
	public static final String PAGE_INDEX = "path.page.index";
	public static final String PAGE_ERROR = "path.page.error";
	public static final String PAGE_VIEW_NEWS_ACTION = "path.action.viewnews";
	public static final String PAGE_VIEW_NEWS_LIST_ACTION = "path.action.viewnewslist";

	public static final String PARAM_LOCALE = "locale";
	public static final String PARAM_NEWS_ID = "newsId";
	public static final String PARAM_AUTHOR_ID = "authorId";
	public static final String PARAM_TAGS = "tags";
	public static final String PARAM_COMMENT_TEXT = "commentText";
	public static final String PARAM_PAGE = "page";
	public static final String PARAM_ACTION = "action";

	public static final String COOKIE_OFFSET = "timezoneOffset";
}
