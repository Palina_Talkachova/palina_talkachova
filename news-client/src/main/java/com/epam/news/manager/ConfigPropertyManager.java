package com.epam.news.manager;

import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ConfigPropertyManager ������
 * 
 * �ets a property
 * 
 * @author Palina_Talkachova
 *
 */
@Component
public class ConfigPropertyManager {

	@Autowired
	private ResourceBundle resourceBundle;

	/**
	 * gets a property by the name (from ClientApplicationConfig class)
	 * 
	 * @param key
	 * @return a property
	 */
	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
