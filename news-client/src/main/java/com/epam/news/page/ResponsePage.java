package com.epam.news.page;

/**
 * ResponsePage class
 * 
 * Consists information about a page
 * 
 * @author Palina_Talkachova
 *
 */
public class ResponsePage {

	private String page;
	private boolean isRedirect;

	public ResponsePage(String page, boolean isRedirect) {
		this.page = page;
		this.isRedirect = isRedirect;
	}

	/**
	 * checks if need to redirect the request
	 *
	 * @return boolean
	 */
	public boolean isRedirect() {
		return isRedirect;
	}

	@Override
	public String toString() {
		return page;
	}
}
